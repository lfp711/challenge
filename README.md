# Word Count of Large File
---
A distributed system for counting unique word of large file.
## Usage
---
1. Process data
    
    Process data and merge the result of each file block automatically at the final step
    
    ```
    java -jar platform-1.0.jar -source dump.xml --spring.data.mongodb.host=192.168.99.100:32768
    ```
    
    Process data in the specified block
    
    ```
    java -jar platform-1.0.jar -source dump.xml -block 100 --spring.data.mongodb.host=192.168.99.100:32768
    ```
    
    Merge the result of each file block manually
    
    ```
    java -jar platform-1.0.jar -merge force --spring.data.mongodb.host=192.168.99.100:32768
    ```
    
1. Initialize or clean mongodb

    ```
    java -jar platform-1.0.jar -init --spring.data.mongodb.host=192.168.99.100:32768
    ```
    
1. Report the count of a word, top 50 most common words and top 50 least common words 

    ```
    java -jar query-1.0.jar -query links --spring.data.mongodb.host=192.168.99.100:32768
    ```
    
    ```
    java -jar query-1.0.jar -most --spring.data.mongodb.host=192.168.99.100:32768
    ```
    
    ```
    java -jar query-1.0.jar -least --spring.data.mongodb.host=192.168.99.100:32768
    ```

## Features
---
1. The platform.jar can be executed in any number of servers to distribute the workload. 
1. It splits a large file to blocks according to the specified size by a parameter file.block.size in command line or spring properties.
1. At the split point between two blocks, it can correct the wrong truncation of a word.
1. A file-based dictionary is used to judge if a string is a word.
1. A distributed lock based on db's table is used in task dispatch.
1. Batch save is used in save objects to mongodb.
1. if it fails in one server when processing data in one file block, the data for this block can be processed dependently by specify the block id.

## Technologies
---
1. Java, Spring Boot, Spring Data MongoDB
1. MongoDB
1. Gradle
1. git, https://bitbucket.org/
1. Apache Commons CLI
1. [File-based Word Dictionary](https://github.com/dwyl/english-words)