package lfp.challenge.platform;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class Lock {
	@Id
	private String id;

	private Date expireAt;

	public Lock() {
		super();
	}

	public Lock(String id, Date expireAt) {
		super();
		this.id = id;
		this.expireAt = expireAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getExpireAt() {
		return expireAt;
	}

	public void setExpireAt(Date expireAt) {
		this.expireAt = expireAt;
	}

	@Override
	public String toString() {
		return "Lock [id=" + id + ", expireAt=" + expireAt + "]";
	}

}