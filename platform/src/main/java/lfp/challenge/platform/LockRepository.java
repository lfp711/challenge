package lfp.challenge.platform;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface LockRepository extends MongoRepository<Lock, String> {

}
