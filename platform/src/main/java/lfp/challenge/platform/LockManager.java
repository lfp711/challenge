package lfp.challenge.platform;

import java.util.Date;
import java.util.Optional;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LockManager {
	String lockId = "WordCount";
	long timeout = 60;
	long tryCount = 30;

	@Autowired
	private LockRepository lRepo;
	private Random rand = new Random(System.currentTimeMillis());

	public boolean acquire() {
		try {
			Thread.sleep(rand.nextInt(100));
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < tryCount; i++) {
			Optional<Lock> lock = lRepo.findById(lockId);
			if (lock.isEmpty()) {
				createLock();
				return true;
			}

			Date currentTime = new java.util.Date();
			Date expireAt = lock.get().getExpireAt();
			if (currentTime.getTime() > expireAt.getTime()) {
				createLock();
				return true;
			}

			try {
				Thread.sleep(rand.nextInt(2000));
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
	}

	private void createLock() {
		lRepo.deleteAll();
		Date currentTime = new java.util.Date();
		Date expireAt = new java.util.Date(currentTime.getTime() + timeout * 1000);

		Lock lock = new Lock(lockId, expireAt);

		lRepo.save(lock);
	}

	public void release() {
		lRepo.deleteAll();
	}

}
