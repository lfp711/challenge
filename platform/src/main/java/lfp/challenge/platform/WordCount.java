package lfp.challenge.platform;

import org.springframework.data.annotation.Id;

public class WordCount {
	@Id
	private String word;
	private Long count;

	public WordCount() {
		super();
	}

	public WordCount(String word, Long count) {
		super();
		this.word = word;
		this.count = count;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "WordCount [word=" + word + ", count=" + count + "]";
	}

}