package lfp.challenge.platform;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.util.StringUtils;

@SpringBootApplication
public class PlatformApplication implements CommandLineRunner {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Value("${file.block.size:20000000}")
	private int blockSize;

	@Autowired
	private LockManager lockManager;

	@Autowired
	MongoTemplate mongoTemplate;

	private String pattern = "[\\p{Punct}\\s+]";

	private String serverTag = UUID.randomUUID().toString();
	private int batchSize = 500;

	private String dictionary = null;

	public static void main(String[] args) {
		SpringApplication.run(PlatformApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		String sourceFile = null;
		boolean init = false;
		String merge = "auto";
		Long blockId = null;
		// Parse command line arguments
		CommandLineParser parser = new DefaultParser();

		Options options = new Options();
		options.addOption(
				Option.builder("source").hasArg().required(false).desc("source file to be processed").build());
		options.addOption(Option.builder("init").hasArg(false).required(false).desc("initialize mongodb").build());
		options.addOption(Option.builder("merge").hasArg(true).required(false)
				.desc("merge word count result. auto, force, no").build());
		options.addOption(Option.builder("block").hasArg(true).required(false).desc("file block id").build());
		options.addOption(Option.builder("dictionary").hasArg(true).required(false).desc("dictionary file").build());

		try {
			// parse the command line arguments
			CommandLine line = parser.parse(options, args, true);
			if (line.hasOption("source")) {
				sourceFile = line.getOptionValue("source");
			}
			if (line.hasOption("init")) {
				init = true;
			}
			if (line.hasOption("merge")) {
				merge = line.getOptionValue("merge");
			}
			if (line.hasOption("block")) {
				try {
					blockId = Long.parseLong(line.getOptionValue("block"));
				} catch (Exception e) {
				}
			}
			if (line.hasOption("dictionary")) {
				dictionary = line.getOptionValue("dictionary");
			}
		} catch (ParseException exp) {
			logger.error("Unexpected exception", exp);
			System.exit(1);
		}

		logger.info("Arguments: sourceFile = {}, init = {}, merge = {}, blockId = {}, dictionary = {}", sourceFile,
				init, merge, blockId, dictionary);
		if (init) {
			init();
			System.exit(0);
		}
		if ("force".equals(merge)) {
			merge();
			System.exit(0);

		}

		long position = 0;
		long additionalSkip = 0;
		long lastBlockId = 0;
		boolean finished = false;
		boolean begin = true;
		try (BufferedReader reader = new BufferedReader(new FileReader(new File(sourceFile)))) {
			// read the file in a loop.
			do {
				long start = System.currentTimeMillis();
				FileBlock fileBlock = null;
				// get the next file block which is available
				if (blockId == null) {
					fileBlock = createBlock();
					if (fileBlock == null) { // try again
						fileBlock = createBlock();
					}
				} else {
					// if a block id is specified in arguments, delete data related to this block
					fileBlock = mongoTemplate.findById(blockId, FileBlock.class);
					if (fileBlock != null) {
						fileBlock.setStartTime(new java.util.Date());
						mongoTemplate.remove(fileBlock);

						Query query = new Query();
						query.addCriteria(Criteria.where("fileBlockId").is(blockId));
						mongoTemplate.remove(query, FileBlockCount.class);
					} else {
						break;
					}
				}
				logger.info("last = {}, current block = {}", lastBlockId, fileBlock);

				// compute the position of this file block and go there
				long skip = (fileBlock.getId() - lastBlockId - (begin ? 0 : 1)) * blockSize - additionalSkip;
				if (skip < 0) {
					skip = 0;
				}
				long actualSkip = reader.skip(skip);
				begin = false;
				position += actualSkip;

				logger.info("begin position = {}, skip = {}, actualSkip = {}", position, skip, actualSkip);
				// block is available
				if (actualSkip == skip) {
					fileBlock.setBeginPosition(position);
					mongoTemplate.save(fileBlock);

					int buffLength = blockSize - (int) additionalSkip;
					char[] cbuf = new char[buffLength];
					int numCharsRead = reader.read(cbuf, 0, buffLength);
					logger.info("numCharsRead = {}", numCharsRead);

					// readable
					if (numCharsRead > -1) {
						position += numCharsRead;
						StringBuilder buffer = new StringBuilder();
						buffer.append(cbuf, 0, numCharsRead);

						StringBuilder firstWord = new StringBuilder();
						// read more character in order to truncate word correctly
						additionalSkip = 0;
						int read = -1;
						while ((read = reader.read()) > -1) {
							additionalSkip++;
							char c = (char) read;
							firstWord.append(c);
							if (("" + c).matches(pattern)) {
								break;
							} else {
								buffer.append(c);
							}
						}
						position += additionalSkip;
						logger.info("end position = {}, additionalSkip = {}, first word in next block = {}", position,
								additionalSkip, firstWord);

						// split the string to an array
						String[] words = buffer.toString().split(pattern);
						logger.info("words[{}] = {}, words[0] = {}", words.length - 1, words[words.length - 1],
								words[0]);
						if (skip > 0 && words != null && words.length > 0) {
							words[0] = "";
						}

						// count each word
						Map<String, Long> counts = Stream.of(words).filter(w -> isWord(w))
								.collect(Collectors.toConcurrentMap(w -> w, w -> 1L, Long::sum));
						logger.info("toConcurrentMap() finished");

						// batch save to mongodb
						int wordTotal = 0;
						List<FileBlockCount> fbcs = new ArrayList<>();
						for (String word : counts.keySet()) {
							if (StringUtils.hasText(word)) {
								FileBlockCount fbc = new FileBlockCount("" + fileBlock.getId() + ":" + word,
										fileBlock.getId(), word, counts.get(word));

								fbcs.add(fbc);
								if (fbcs.size() > batchSize) {
									mongoTemplate.insertAll(fbcs);
									fbcs.clear();
								}
								wordTotal++;
							}
						}
						if (fbcs.size() > 0) {
							mongoTemplate.insertAll(fbcs);
							fbcs.clear();
						}
						logger.info("saving FileBlockCount to db finished, total = {}", wordTotal);

						fileBlock.setEndPosition(position);
						fileBlock.setStopTime(new java.util.Date());
						mongoTemplate.save(fileBlock);

						// last block
						if (numCharsRead < buffLength) {
							logger.info("last block");
							if ("auto".equals(merge) && blockId == null) {
								merge();
								finished = true;
							}
						}
					} else {
						finished = true;
					}
				} else {
					// end
					finished = true;
					logger.info("end");
				}

				lastBlockId = fileBlock.getId();
				logger.info("elapsed = {}", System.currentTimeMillis() - start);
			} while (!finished && blockId == null);
		}
	}

	private void init() {
		mongoTemplate.dropCollection(FileBlock.class);
		mongoTemplate.dropCollection(FileBlockCount.class);
		mongoTemplate.dropCollection(WordCount.class);
		mongoTemplate.dropCollection(Lock.class);

		Query query = new Query();
		query.addCriteria(Criteria.where("id").exists(true));

		mongoTemplate.remove(query, FileBlock.class);
		mongoTemplate.remove(query, FileBlockCount.class);
		mongoTemplate.remove(query, WordCount.class);
		mongoTemplate.remove(query, Lock.class);
	}

	private FileBlock createBlock() {
		FileBlock fileBlock = null;
		// a distributed lock based on db's table
		if (lockManager.acquire()) {
			long maxBlockId = -1;
			Query query = new Query();
			query.with(Sort.by(new Order(Direction.DESC, "id")));
			query.limit(1);
			List<FileBlock> fbs = mongoTemplate.find(query, FileBlock.class);
			if (fbs != null && fbs.size() > 0) {
				maxBlockId = fbs.get(0).getId();
			}

			long blockId = maxBlockId + 1;
			fileBlock = new FileBlock(blockId, new java.util.Date(), null, null, null);
			fileBlock.setServerTag(serverTag);
			mongoTemplate.save(fileBlock);

			lockManager.release();
		}
		return fileBlock;
	}

	private void merge() {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").exists(true));
		mongoTemplate.remove(query, WordCount.class);
		mongoTemplate.dropCollection(WordCount.class);

		Aggregation agg = newAggregation(group("word").sum("count").as("count"), sort(Sort.Direction.DESC, "count"))
				.withOptions(AggregationOptions.builder().allowDiskUse(true).build());
		AggregationResults<WordCount> groupResults = mongoTemplate.aggregate(agg, FileBlockCount.class,
				WordCount.class);
		List<WordCount> result = groupResults.getMappedResults();

		List<WordCount> wcs = new ArrayList<>();
		for (WordCount wc : result) {
			if (isWord(wc.getWord())) {
				wcs.add(wc);
				if (wcs.size() > batchSize) {
					mongoTemplate.insertAll(wcs);
					wcs.clear();
				}
			}
		}
		if (wcs.size() > 0) {
			mongoTemplate.insertAll(wcs);
			wcs.clear();
		}

		logger.info("merge finished");
	}

	private boolean initWords = false;
	private Set<String> words = new HashSet<>();

	// check if the string is a word by a file-based dictionary
	private boolean isWord(String str) {
		if (!initWords) {
			try {
				BufferedReader bReader = null;
				if (dictionary == null) {
					bReader = new BufferedReader(
							new InputStreamReader(this.getClass().getResourceAsStream("/words.txt")));
				} else {
					bReader = new BufferedReader(new FileReader(new File(dictionary)));
				}

				String word = null;
				while ((word = bReader.readLine()) != null) {
					words.add(word);
				}
				bReader.close();
			} catch (Exception e) {

			}
			logger.info("words.size() = {}", words.size());
			initWords = true;
		}

		if (StringUtils.hasText(str) && words.contains(str)) {
			return true;
		}
		return false;
	}

}
