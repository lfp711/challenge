package lfp.challenge.platform;

import org.springframework.data.annotation.Id;

public class FileBlockCount {
	@Id
	private String id;

	private Long fileBlockId;
	private String word;
	private Long count;

	public FileBlockCount() {
		super();
	}

	public FileBlockCount(String id, Long fileBlockId, String word, Long count) {
		super();
		this.id = id;
		this.fileBlockId = fileBlockId;
		this.word = word;
		this.count = count;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getFileBlockId() {
		return fileBlockId;
	}

	public void setFileBlockId(Long fileBlockId) {
		this.fileBlockId = fileBlockId;
	}

	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}

	public Long getCount() {
		return count;
	}

	public void setCount(Long count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "FileBlockCount [id=" + id + ", fileBlockId=" + fileBlockId + ", word=" + word + ", count=" + count
				+ "]";
	}

}