package lfp.challenge.platform;

import java.util.Date;

import org.springframework.data.annotation.Id;

public class FileBlock {
	@Id
	private Long id;
	private String serverTag;

	private Date startTime;
	private Date stopTime;
	private Long beginPosition;
	private Long endPosition;

	public FileBlock() {
		super();
	}

	public FileBlock(Long id, Date startTime, Date stopTime, Long beginPosition, Long endPosition) {
		this(id, null, startTime, stopTime, beginPosition, endPosition);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getStopTime() {
		return stopTime;
	}

	public void setStopTime(Date stopTime) {
		this.stopTime = stopTime;
	}

	public Long getBeginPosition() {
		return beginPosition;
	}

	public void setBeginPosition(Long beginPosition) {
		this.beginPosition = beginPosition;
	}

	public Long getEndPosition() {
		return endPosition;
	}

	public void setEndPosition(Long endPosition) {
		this.endPosition = endPosition;
	}

	public String getServerTag() {
		return serverTag;
	}

	public void setServerTag(String serverTag) {
		this.serverTag = serverTag;
	}

	public FileBlock(Long id, String serverTag, Date startTime, Date stopTime, Long beginPosition, Long endPosition) {
		super();
		this.id = id;
		this.serverTag = serverTag;
		this.startTime = startTime;
		this.stopTime = stopTime;
		this.beginPosition = beginPosition;
		this.endPosition = endPosition;
	}

	@Override
	public String toString() {
		return "FileBlock [id=" + id + ", serverTag=" + serverTag + ", startTime=" + startTime + ", stopTime="
				+ stopTime + ", beginPosition=" + beginPosition + ", endPosition=" + endPosition + "]";
	}

}