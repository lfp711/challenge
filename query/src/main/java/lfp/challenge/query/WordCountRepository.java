package lfp.challenge.query;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;


public interface WordCountRepository extends MongoRepository<WordCount, String> {
	WordCount findByWord(String word);

	List<WordCount> findTop50ByOrderByCountDesc();

	List<WordCount> findTop50ByOrderByCountAsc();

}
