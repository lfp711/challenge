package lfp.challenge.query;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.util.StringUtils;

@SpringBootApplication
public class QueryApplication implements CommandLineRunner {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public static void main(String[] args) {
		SpringApplication.run(QueryApplication.class, args);
	}

	@Autowired
	private WordCountRepository wcRepo;

	@Override
	public void run(String... args) throws Exception {
		String query = "";
		boolean most = false;
		boolean least = false;

		CommandLineParser parser = new DefaultParser();

		Options options = new Options();
		options.addOption(Option.builder("query").hasArg(true).required(false).desc("query word count").build());
		options.addOption(
				Option.builder("most").hasArg(false).required(false).desc("top 50 most common words").build());
		options.addOption(
				Option.builder("least").hasArg(false).required(false).desc("top 50 least common words").build());

		try {
			// parse the command line arguments
			CommandLine line = parser.parse(options, args, true);
			if (line.hasOption("query")) {
				query = line.getOptionValue("query");
			}
			if (line.hasOption("most")) {
				most = true;
			}
			if (line.hasOption("least")) {
				least = true;
			}
		} catch (ParseException exp) {
			logger.error("Unexpected exception", exp);
			System.exit(1);
		}
		System.out.println("======================================");

		if (StringUtils.hasText(query)) {
			WordCount wc = wcRepo.findByWord(query);
			if (wc != null) {
				System.out.println("\t" + wc.getWord() + ": " + wc.getCount());
			}
		}
		if (most) {
			int i = 1;
			for (WordCount wc : wcRepo.findTop50ByOrderByCountDesc()) {
				System.out.println("\t" + (i++) + ". " + wc.getWord() + ": " + wc.getCount());
			}
		}
		if (least) {
			int i = 1;
			for (WordCount wc : wcRepo.findTop50ByOrderByCountAsc()) {
				System.out.println("\t" + (i++) + ". " + wc.getWord() + ": " + wc.getCount());
			}
		}
		System.out.println();

	}

}
